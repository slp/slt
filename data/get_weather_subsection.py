import os
import json
import re
import numpy as np
root = "transcripts/"
jsons = os.listdir(root)
import matplotlib.pyplot as plt
import pandas as pd

def moving_average(a, n=50) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n
    
    
#check if one of the word announcing the start of the weather section is in the given sentence
def find_word(sentence):
	if "wettervorhersage" in sentence.lower():			return 1
	if "wetteraussichten" in sentence.lower(): 			return 1
	if "wettervorsage" in sentence.lower():			return 1
	if "wir kommen zum Wetter" in sentence.lower():		return 1
	if "wetterhersage" in sentence.lower():			return 1
	if "die vorhersage für morgen" in sentence.lower():		return 1
	if "die wetter-vorhersage für morgen" in sentence.lower():	return 1
	
	return 0
	
#detect if we are on the start frame for the files not catched by find_word so we can add the frame manualy
def missing(name,index):
	if name == "TV-20200316-2057-0300" and index == "207":	return 1
	if name == "TV-20180311-2036-0901" and index == "213":	return 1
	if name == "TV-20181209-2036-3801" and index == "221":	return 1
	if name == "TV-20210301-2037-0300" and index == "253":	return 1
	if name == "TV-20180412-2042-0401" and index == "212":	return 1
	if name == "TV-20200522-2026-3500" and index == "192":	return 1
	if name == "TV-20180920-2046-0401" and index == "222":	return 1
	if name == "TV-20180817-2038-1501" and index == "225":	return 1		
	if name == "TV-20200305-2027-2100" and index == "189":	return 1
	if name == "TV-20180104-2041-0201" and index == "236":	return 1
	if name == "TV-20181218-2034-5001" and index == "198":	return 1
	if name == "TV-20181208-2040-4301" and index == "218":	return 1
	if name == "TV-20200225-2025-5400" and index == "228":	return 1
	if name == "TV-20200310-2223-5500" and index == "212":	return 1
	if name == "TV-20200117-2035-0101" and index == "231":	return 1
	if name == "TV-20190225-2038-0201" and index == "212":	return 1 
	if name == "TV-20191224-2059-4001" and index == "225":	return 1
	if name == "TV-20210721-2029-3900" and index == "234":	return 1
	
	return 0
                

#FINDING FRAME NUMBER OF SPEECH START 

starts = {}
years = {}
names = {}
sentences = {}
num_frames = {}
detected = {}
time = {}
num_file = 0
num_missing_files = 0

#for each file
for js in jsons:
    year = js.split("-")[1][:4]
    if not year in years:
        years[year] = 0
    years[year] += 1
    
    #read clip json
    folder = js.split("_")[0]
    if not folder in starts:
        starts[folder] = ''
    aas = False
    begginer = ""
    
    with open(os.path.join(root, js), 'r') as f:
        content = json.load(f)
        s__start = 0.0
        w_start = 0.0
        
        #get duration of clip
        raw_index = list(content.keys()).index("raw")
        
        names[num_file] = js[0:21]
        detected[num_file] = 0
        num_frames[num_file] = -1
        
        corrupted = ["TV-20190824-2053-5701","TV-20191109-2106-2001","TV-20200316-2057-0300","TV-20190810-2034-0601","TV-20200405-2042-5800"]
        	
        if names[num_file] in corrupted:	#corrupted file
        	detected[num_file] = 1
       
       #for each sentence of the file
        for index in list(content.keys()):
            if not index in ["raw", "seq_len"]:
            
            	#first test: we detect the start by searching specific words
            	#second test: for the special cases, we add the frame manualy
                if (find_word(content[index]["txt"]) and detected[num_file] == 0) or missing(names[num_file],index):
                	_, min, sec = content[index]["start"].split(":")
                	_ms = float(sec)*1000
                	_sec = float(min)*60
                	_ms = _ms + (_sec*1000)
                	w_start = (_ms*0.025)		#for 0.025 fpms
                	
                	time[num_file] = "00:" + str(min) + ":" + str(sec)
                	num_frames[num_file] = w_start
                	sentences[num_file] = content[index]["txt"].replace('"',"")
                	detected[num_file] = 1
                	
                	#print(names[num_file] , ": start at frame" , w_start, "and time" , content[index]["start"], "\n")
                		
                    
                    
        #we count and print the files we did not catch earlier
        if detected[num_file] == 0:
        	num_missing_files = num_missing_files + 1
        	print(names[num_file])
        num_file = num_file + 1

print(num_missing_files , "files are not detected")


#WRITING THE DATA IN A JSON FILE

content = {}
for i in range(num_file):
	if(num_frames[i] >= 0):	content[names[i]] = int(num_frames[i])
with open("jsons/starts.json", 'w') as f:
	json.dump(content,f,indent = 2)
	
	
#WRITING SECOND JSON FILE FOR PARSING WITH BASH LATER

data = ''
data_list = []
for i in range(num_file):
	if(num_frames[i] >=0):
		data = '{"name": "' + names[i] + '", "frame": ' + str(int(num_frames[i])) + ', "time": "' + time[i] + '", "sentence": "' + sentences[i] + '"}'
		data_list.append(json.loads(data))
	
with open("jsons/starts2.json", 'w') as f:
	json.dump(data_list,f,indent = 2)
	
	





                    
                    
                    

