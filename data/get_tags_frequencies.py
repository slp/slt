#CODE TO FIND RELEVANT TAGS IN MAIN SECTION

from transformers import AutoModel, AutoTokenizer, AutoModelForTokenClassification
from flair.models import SequenceTagger
from transformers import pipeline
from flair.data import Sentence
import numpy
import json
import os

#read a json file and return its data
def read_json(path):
  #read json and return content
  with open(path, 'r') as f:
      data = json.load(f)
  return data
 
  
#convert the time of a frame to an int number
def convert_time2frame(unit_index):
    if "position:95%align:end" in unit_index:
      unit_index = unit_index.replace("position:95%align:end", "")
    _, min, sec = unit_index.split(":")
    _ms = float(sec)*1000
    _sec = float(min)*60
    _ms = _ms + (_sec*1000)
    frame = (_ms*0.025)
    return frame 
    

def preprocess_function(text):
    tokenizer = AutoTokenizer.from_pretrained("dbmdz/bert-base-german-cased")
    return tokenizer(text, truncation=True)


#add the tags of that sample to the tags list
def get_tags(sample_id,sentences_dict,tagger,tags):

  if sample_id not in sentences_dict:
    return;

  #for each sentence of the sample: getting the tags
  for sen in sentences_dict[sample_id]:
    pool = sentences_dict[sample_id][sen]["pool"]
    if pool == "main":
      sentence = Sentence(sen)
      tagger.predict(sentence)
      
      #parsing the tagged sentence to find each tag
      tagged_sentence = sentence.to_tagged_string()
      list_elem = tagged_sentence.split()
      list_tags = list_elem[1::2]
      for t in list_tags:
        if t not in tags:
          tags[t] = 0
          
        tags[t] += 1

def main():  
  #load weather start frame dict
  weather_data = read_json("jsons/starts2.json")

  sentences_dict = read_json("jsons/total_sentences_incl_ends.json")
  tagger = SequenceTagger.load("flair/upos-multi")
  tags = {}

  #for each sample
  for u in weather_data:
    #add the tags found in this sample to the list of tags
    get_tags(u["name"],sentences_dict,tagger,tags)
    
  #write it in a json file
  with open("jsons/tags_frequencies.json", 'w') as f:
    json.dump(tags,f,indent = 2)


main()
		
