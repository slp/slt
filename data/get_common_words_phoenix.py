from transformers import AutoModel, AutoTokenizer, AutoModelForTokenClassification
from flair.models import SequenceTagger
from transformers import pipeline
from flair.data import Sentence
from collections import Counter
import matplotlib.pyplot as plt
import json
import os


#read a json file and return its data
def read_json(path):
  #read json and return content
  with open(path, 'r') as f:
      data = json.load(f)
  return data
  

#find the 10 most common words between the phoenix corpus and ours and plot them  
def plot_most_common_words(tag,words,phoenix_words):
  #sorting the lists by words frequency and deleting duplicates
  words_and_frequencies = Counter(words).most_common()
  words = [i[0] for i in words_and_frequencies]
  
  phoenix_words_and_frequencies = Counter(phoenix_words).most_common()
  phoenix_words = [i[0] for i in phoenix_words_and_frequencies]
  
  #finding common words 
  s = set(phoenix_words)
  common_words = [x for x in words if x in s]
  
  #we keep only the 10 first common_words
  n = min(10,len(common_words))
  common_words = common_words[:n]
  
  #getting their global frequency
  words_and_frequencies = dict(words_and_frequencies)
  phoenix_words_and_frequencies = dict(phoenix_words_and_frequencies)
  
  frequencies = []
  for w in common_words:
    frequencies.append(words_and_frequencies[w] + phoenix_words_and_frequencies[w])

  #plotting
  fig = plt.figure(figsize = (10,5))
  plt.bar(common_words,frequencies,color="red",width=0.4)
  plt.xlabel("Most common words")
  plt.ylabel("Frenquencies")
  plt.title(tag)
  plt.show()
  
  filename = "plots/common_" + tag + ".png"
  plt.savefig(filename)


#words from phoenix corpus
phoenix_NOUN = []
phoenix_SYM = []
phoenix_ADP = []
phoenix_ADV = []
phoenix_PROPN = []
phoenix_VERB = []

#words from our corpus
NOUN = []
SYM = []
ADP = []
ADV = []
PROPN = []
VERB = []


#First, we group the words in lists depending on their corpus (ours or phoenix) and tags (NOUN, ADP, VERB, etc)


sentences_dict = read_json("jsons/total_sentences.json")
phoenix_sentences_dict = read_json("jsons/phoenix_sentences.json")

#load tagger
tagger = SequenceTagger.load("flair/upos-multi")

#getting the words of our corpus
print("Collecting words of our corpus...")
for sample_id in sentences_dict:
  for sen in sentences_dict[sample_id]:
    sentence = Sentence(sen)
    tagger.predict(sentence)
    
    #parsing the tagged sentence to find each token and its tag
    tagged_sentence = sentence.to_tagged_string()

    list_elem = tagged_sentence.split()
    words, tags = list_elem[0::2], list_elem[1::2]
    for n, t in enumerate(tags):
      if t == "<NOUN>":
        NOUN.append(words[n])
      if t == "<SYM>":
        SYM.append(words[n])
      if t == "<ADP>":
        ADP.append(words[n])
      if t == "<ADV>":
        ADV.append(words[n])
      if t == "<PROPN>":
        PROPN.append(words[n])
      if t == "<VERB>":
        VERB.append(words[n])
        
#getting the words of phoenix corpus
print("Collecting words of phoenix corpus...")
for _,sen in sentences.items():
    sentence = Sentence(sen)
    tagger.predict(sentence)
    
    #parsing the tagged sentence to find each token and its tag
    tagged_sentence = sentence.to_tagged_string()

    list_elem = tagged_sentence.split()
    words, tags = list_elem[0::2], list_elem[1::2]
    for n, t in enumerate(tags):
      if t == "<NOUN>":
        phoenix_NOUN.append(words[n])
      if t == "<SYM>":
        phoenix_SYM.append(words[n])
      if t == "<ADP>":
        phoenix_ADP.append(words[n])
      if t == "<ADV>":
        phoenix_ADV.append(words[n])
      if t == "<PROPN>":
        phoenix_PROPN.append(words[n])
      if t == "<VERB>":
        phoenix_VERB.append(words[n])


#For each, tag, we find the 10 most common words between the phoenix corpus and ours and plot them


print("Plotting the 10 most common words for each tag...")
plot_most_common_words("NOUN", NOUN, phoenix_NOUN)
plot_most_common_words("SYM", SYM, phoenix_SYM)
plot_most_common_words("ADP", ADP, phoenix_ADP)
plot_most_common_words("ADV", ADV, phoenix_ADV)
plot_most_common_words("PROPN", PROPN, phoenix_PROPN)
plot_most_common_words("VERB", VERB, phoenix_VERB)












