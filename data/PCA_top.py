from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from pandas import DataFrame
import mediapipe as mp
import pandas as pd
import numpy as np
import cv2

###Use PCA to reduce the number of landmarks for the face###

#Getting the manualy pre-selected landmarks for the top part
top = []

with open('top.txt', 'r', encoding='ISO-8859-1') as f:
    liste = f.read().split(',')
    for elem in liste:
        subelem = elem.split('_')
        l = subelem[1]
        l = int(l[:-1])
        top.append(l)

print("Landmarks of the top part: ", top)

#Getting the data and converting it in a DataFrame...


print("Collecting the data...")

data = pd.DataFrame(columns=list(range(0,len(top))))     #empty dataframe
frame = []                                          #list with all the landmarks of one frame
top_frame = []
landmark = 0                                        #one landmark is the sum of x,y and z coordinates
index = 0                                           #index of the sentence

with open('E://output/signer_b_face.txt', 'r', encoding='ISO-8859-1') as f:
    for line in f:
        print("Number of data (frames with landmarks) after this line:",index)
        #if index > 10000:   #we don't need all the data
        #    break
        frame = []
        words = line.split()
        #We only treat lines containing landmarks (not the sentences)
        if words[0] == "A:":  
            for i in range(1,len(words)-2):
                landmark = float(words[i]) + float(words[i+1]) + float(words[i+2])     #x,y,z coordinates
                frame.append(landmark)
                i = i + 2
                if(len(frame)==468):                            #each frame has 468 landmarks
                    for j in top:
                        top_frame.append(frame[j])
                    data.loc[index] = top_frame
                    frame = []
                    top_frame = []
                    index += 1
                    
data.to_pickle("E://output/landmarks_top_dataframe.pkl")                       #we save the DataFrame

#...Or loading the previously created DataFrame

#data = pd.read_pickle("/mnt/e/output/landmarks_top_dataframe.pkl")


#Getting the number of principal component needed and doing PCA


#Normalizing the data
normalized_data=(data-data.min())/(data.max()-data.min())

#95% of variance
pca = PCA(0.95)
pca.fit(normalized_data)

print("Number of components needed:" , pca.n_components_)

#Getting the names (numbers) of the principal components selected
model = normalized_data
n_pcs= pca.n_components_
most_important = [np.abs(pca.components_[i]).argmax() for i in range(n_pcs)]

initial_feature_names = list(range(0,468))

most_important_names = [initial_feature_names[most_important[i]] for i in range(n_pcs)]
dic = {'PC{}'.format(i+1): most_important_names[i] for i in range(n_pcs)}
df = pd.DataFrame(sorted(dic.items()))

principal_components = list(set(df[1].tolist()))
principal_components.sort()
print("Principal components:",principal_components)


#visualizing the landmarks selected

image = cv2.imread("image2.jpg")
mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh()
rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
result = face_mesh.process(rgb_image)

height, width, _ = image.shape
for facial_landmarks in result.multi_face_landmarks:
    for i in principal_components:
        pt1 = facial_landmarks.landmark[i]
        x = int(pt1.x * width)
        y = int(pt1.y * height)
        cv2.circle(image, (x, y), 2, (255,255,255), -1)

cv2.imwrite("landmarks_top_pca.jpg", image)

