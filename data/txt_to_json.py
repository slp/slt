import json

ends = {}

with open("weather_ends.txt","r") as data:
	for line in data:
		list_line = line.split()
		ends[list_line[1]] = int(list_line[0])

with open("ends.json","w") as f:
  json.dump(ends,f,indent = 2)
