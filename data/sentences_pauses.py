import matplotlib.pyplot as plt
import os
import json

#read a json file and return its data
def read_json(path):
  #read json and return content
  with open(path, 'r') as f:
      data = json.load(f)
  return data
  
#getting the list of samples to use i.e only the ones with the selected signer
samples_list = []
for filename in os.listdir("target_signer/B"):
  #deleting the "_cropped.jpg"
  filename = filename[:-12]
  samples_list.append(filename)
  
#getting the pauses between two sentences for our dataset

data = read_json("jsons/total_sentences_incl_ends_3.json")
pauses = {}

#for each sample
for sample in data:
  if sample in samples_list:
    prev_end = -1   #end of the previous sentence
    pause = 0
    for sentence in data[sample]:
      if prev_end > -1:
        pause = int(sentence[1][0] - prev_end)
        if pause < 0:
          print(sample)
          #print(sentence[1][0])
          #print(prev_end)
        else:
          if pause not in pauses:
            pauses[pause] = 1
          else:
            pauses[pause] = pauses[pause] + 1
      if pause >= 0:
        prev_end = sentence[1][-1]


#write dict in a json file

#sorting it by keys
pauses_dict = pauses
pauses = sorted(pauses.items())

with open("jsons/sentences_pauses_signer_B.json", 'w') as f:
  json.dump(pauses_dict,f,indent = 2)


#plotting it

pauses_list = []
pauses_occurence = []

for p in pauses:
  #focus where most of the pauses are
  if p[0] <= 300 and p[0] >= 25:
    pauses_list.append(p[0])
    pauses_occurence.append(p[1])

#removing the 3 last values
del pauses_list[-3:]
del pauses_occurence[-3:]

#pauses_list = list(pauses.keys())
#pauses_occurence = list(pauses.values())

#plt.plot(pauses_list,pauses_occurence,"r-")
plt.bar(pauses_list,pauses_occurence)
plt.xlabel("Duration of pause in frame")
plt.ylabel("Occurences of pauses")
plt.title("Pauses between sentences")

plt.savefig("plots/pauses_signer_B.png")
plt.show()


#plotting the 10 best

pauses = sorted(pauses_dict.items(), key=lambda x: x[1], reverse=True)

pauses_list = []
pauses_occurence = []

i = 0
for p in pauses:
  if i < 10:
    pauses_list.append(p[0])
    pauses_occurence.append(p[1])
  i = i + 1

#pauses_list = list(pauses.keys())
#pauses_occurence = list(pauses.values())

#plt.plot(pauses_list,pauses_occurence,"o")
plt.bar(pauses_list,pauses_occurence)
plt.xlabel("Duration of pause in frame")
plt.ylabel("Occurences of pauses")
plt.title("Pauses between sentences")

plt.savefig("plots/pauses_10_signer_B.png")
plt.show()




