from transformers import AutoModel, AutoTokenizer, AutoModelForTokenClassification
from flair.models import SequenceTagger
from transformers import pipeline
from flair.data import Sentence
import json
import os


#read a json file and return its data
def read_json(path):
  #read json and return content
  with open(path, 'r') as f:
      data = json.load(f)
  return data
 
  
#convert the time of a frame to an int number
def convert_time2frame(unit_index):
    if "position:95%align:end" in unit_index:
      unit_index = unit_index.replace("position:95%align:end", "")
    _, min, sec = unit_index.split(":")
    _ms = float(sec)*1000
    _sec = float(min)*60
    _ms = _ms + (_sec*1000)
    frame = (_ms*0.025)
    return frame 

    
def preprocess_function(text):
    tokenizer = AutoTokenizer.from_pretrained("dbmdz/bert-base-german-cased")
    return tokenizer(text, truncation=True)


#create a dictionary with, for each file, all its sentences, start and end frame, and the pool (main or weather)
def get_sentences():
  #load weather start frame dict
  weather_data = read_json("jsons/starts2.json")

  #load weather end frame dict
  ends = read_json("jsons/ends.json")
  
  #load sample start frame dict
  samples_start = read_json("jsons/starts_of_sample.json")
  
  dict_samples = {}
  total_sentences = {}

  #for each sample
  for u in weather_data:
  
    #load the json file with the data of this sample
    load_transcript = read_json(os.path.join('transcripts', "{}{}".format(u["name"], '_transcript.json')))
    start_of_sample = samples_start[u["name"]]

    start_collect_w = False
    start_collect_m = False

    if not u["name"] in total_sentences:
      total_sentences[u["name"]] = {}

    if not u["name"] in dict_samples:
      dict_samples[u["name"]] = {"main_pool":{}, "weather_pool":{}}

    sentences = [s for s in load_transcript["raw"].split(".")]
    target_length = load_transcript["seq_len"]
    
    sub_d = {}
    
    #for each sentence
    for sub_u in load_transcript:
      if not sub_u in ["raw", "seq_len"]:
        #get local start/end frames
        start, end = convert_time2frame(load_transcript[sub_u]['start']),\
                     convert_time2frame(load_transcript[sub_u]['end'])

        #check if index is inside start/end
        if start<=float(start_of_sample)<=end:
            #set cmd to collect main
            start_collect_m = True

        #concatenate sentences and find their start and end frame
        if start_collect_m or start_collect_w:
          txt = load_transcript[sub_u]['txt']
          for s in sentences:
            if txt.replace(".", "") in s:
              if not s in sub_d:
                sub_d[s] = {"index":sub_u,
                            "txt":[],
                            "timing":{},
                            "pool":"main" if start_collect_m else "weather"}
                sub_d[s]["txt"].append(txt)
                sub_d[s]["timing"]["start"]  = start
                sub_d[s]["timing"]["end"]  = end
              else:
                sub_d[s]["txt"].append(txt)
                sub_d[s]["timing_end"] = {"end":end}

        #check if weather start is matching unit in transcript
        if load_transcript[sub_u]['start'] == u["time"]:
          start_collect_w = True
          start_collect_m = False

        #check if we reach the end of the weather section
        if u["name"] in ends.keys() and start == ends[u["name"]]:
          start_collect_w = False

    for k in sub_d:
        #check if total sentence is based on sub-sentences
        end_ = sub_d[k]["timing"]["end"]
        if "timing_end" in sub_d[k]:
          end_ = sub_d[k]["timing_end"]["end"]

        total_sentences[u["name"]][k] = {"start":sub_d[k]["timing"]["start"],
                                          "end":end_,
                                          "pool":sub_d[k]["pool"]}

  #after parsing all files, we write in the json file
  with open("jsons/total_sentences.json", 'w') as f:
    json.dump(total_sentences,f,indent = 2)
  

#create the main and weather pools
def get_pools(sample_id,sentences_dict):
  main_pool, weather_pool = [], []
  include_tags = ["<NOUN>" , "<DET>", "<ADP>", "<VERB>"]

  #load tagger
  tagger = SequenceTagger.load("flair/upos-multi")

  from string import punctuation
  symbols = list(punctuation)

  local_cache = {}
  #for each sentence of the sample: getting the words and their tag
  for sen in sentences_dict[sample_id]:
    
    sentence = Sentence(sen)
    tagger.predict(sentence)
    
    #parsing the tagged sentence to find each token and its tag
    tagged_sentence = sentence.to_tagged_string()
    pool = sentences_dict[sample_id][sen]["pool"]

    list_elem = tagged_sentence.split()
    words, tags = list_elem[0::2], list_elem[1::2]
    for n, t in enumerate(tags):
      if not t in local_cache:
        local_cache[t] = {"main":[], "weather":[]}
      if not words[n] in local_cache[t][pool]:
        local_cache[t][pool].append(words[n])

  for tag in include_tags:
    main_pool += local_cache[tag]["main"]
    weather_pool += local_cache[tag]["weather"]

  return main_pool, weather_pool
  

#sort main and weather pool and find common words based on their frequency
def compare_words(main_pool, weather_pool):
  list3 = set(main_pool)&set(weather_pool)
  list4 = sorted(list3)
  
  print("COMMON WORDS:", list4[:20])
  return list4[:20]


def main():  
  #load weather start frame dict
  weather_data = read_json("jsons/starts2.json")
  
  #create json file with sorted sentences
  get_sentences()

  sentences_dict = read_json("jsons/total_sentences.json")

  common_words_dict = {}

  #for each sample
  for u in weather_data:
    
    #create the main and weather pools
    print (u["name"])
    main_pool, weather_pool = get_pools(u["name"],sentences_dict)
    
    #sort main and weather pool and find common words based on their frequency
    common_words = compare_words(main_pool, weather_pool)
    
    #add the common_words of that file in a json dict
    if not u["name"] in common_words_dict:
      common_words_dict[u["name"]] =  {"common_words":{}}

    common_words_dict[u["name"]]["common words"] = common_words
   
  #write dict in a json file
  with open("jsons/common_words.json", 'w') as f:
    json.dump(common_words_dict,f,indent = 2)
  

main()
		
