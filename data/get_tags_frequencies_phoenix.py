from transformers import AutoModel, AutoTokenizer, AutoModelForTokenClassification
from flair.models import SequenceTagger
from transformers import pipeline
from flair.data import Sentence
import numpy
import os
import json 


#add the tags of that sample to the tags list
def get_tags(sentences,tagger,tags):
  
  #for each sentence: getting the tags
  for _,sen in sentences.items():
    sentence = Sentence(sen)
    tagger.predict(sentence)
    
    #parsing the tagged sentence to find each tag
    tagged_sentence = sentence.to_tagged_string()
    list_elem = tagged_sentence.split()
    list_tags = list_elem[1::2]
    for t in list_tags:
      if t not in tags:
        tags[t] = 0
      tags[t] += 1 


#read the csv files and save the sentences (list of glosses) in a json
def get_sentences(sentences):
  path = "phoenix/"
  files = ["dev.corpus.csv", "test.corpus.csv", "train.corpus.csv"]

  for f in files:
    with open(os.path.join(path,f), 'r') as csv_file:
      lines = csv_file.readlines()
      count = 0
      for line in lines:
        if count > 0:				#we ignore the first line, it's not data
          split_line = line.split("|")
          sentence = split_line[3]		#we need the part with the glosses
          sentence = sentence[:-2]		#we delete the "\n" at the end of the sentence
          sentences[str(count-1)] = sentence
        count += 1
 
  with open("jsons/phoenix_sentences.json", 'w') as f:
    json.dump(sentences,f,indent = 2)
    
    
#read the json and write each tag occurence in a json
def tags_frequencies(sentences):
  tagger = SequenceTagger.load("flair/upos-multi")
  tags = {}
  get_tags(sentences,tagger,tags)

  with open("jsons/phoenix_tags_frequencies.json", 'w') as f:
    json.dump(tags,f,indent = 2)


def main():
  sentences = {}
  #Reading the csv files and saving the sentences (list of glosses) in a json
  get_sentences(sentences)
  #Reading the json and writing each tag occurence in a json
  tags_frequencies(sentences)


main()









