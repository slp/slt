from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from pandas import DataFrame
import mediapipe as mp
import pandas as pd
import numpy as np
import cv2

###Use PCA to reduce the number of landmarks for the face###


"""#Getting the data and converting it in a DataFrame...


print("Collecting the data...")

data = pd.DataFrame(columns=list(range(0,468)))     #empty dataframe
frame = []                                          #list with all the landmarks of one frame
landmark = 0                                        #one landmark is the sum of x,y and z coordinates
index = 0                                           #index of the sentence

with open('/mnt/e/output/signer_b_face.txt', 'r', encoding='ISO-8859-1') as f:
    for line in f:
        print("Number of data (frames with landmarks) after this line:",index)
        if index > 10000:   #we don't need all the data
            break;
        frame = []
        words = line.split()
        #We only treat lines containing landmarks (not the sentences)
        if words[0] == "A:":  
            for i in range(1,len(words)-2):
                landmark = float(words[i]) + float(words[i+1])     #x,y coordinates
                frame.append(landmark)
                i = i + 2 
                if(len(frame)==468):                              #each frame has 468 landmarks
                    data.loc[index] = frame
                    frame = []
                    index += 1
                    
data.to_pickle("/mnt/e/output/landmarks_dataframe.pkl")                       #we save the DataFrame

"""
#...Or loading the previously created DataFrame

data = pd.read_pickle("/mnt/e/output/landmarks_dataframe.pkl")

#visualizing the landmarks selected

image = cv2.imread("image2.jpg")
mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh()
rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
result = face_mesh.process(rgb_image)

#key landmaks for mouth and eyes
facial_areas = {
    'Face_oval': mp_face_mesh.FACEMESH_FACE_OVAL
    , 'Left_eye': mp_face_mesh.FACEMESH_LEFT_EYE
    , 'Left_eye_brow': mp_face_mesh.FACEMESH_LEFT_EYEBROW
    , 'Right_eye': mp_face_mesh.FACEMESH_RIGHT_EYE
    , 'Right_eye_brow': mp_face_mesh.FACEMESH_RIGHT_EYEBROW
    , 'Lips': mp_face_mesh.FACEMESH_LIPS
}

landmarks_top = [234,127,162,21,54,103,67,109,10,338,297,332,284,251,389,356,454]
landmarks_bottom = []

top_areas = ['Left_eye','Left_eye_brow','Right_eye','Right_eye_brow']

for facial_area in facial_areas.keys():
    for couple in facial_areas[facial_area]:
        for landmark in couple:
            if facial_area in top_areas:
                landmarks_top.append(landmark)
            else:
                if facial_area != 'Face_oval':
                    landmarks_bottom.append(landmark)
            if facial_area == 'Face_oval' and landmark not in landmarks_top:
                landmarks_bottom.append(landmark)

landmarks_top = list(dict.fromkeys(landmarks_top))      #removing duplicates
landmarks_bottom = list(dict.fromkeys(landmarks_bottom))      #removing duplicates
print("Number of landmarks: ", len(landmarks_top)+len(landmarks_bottom))

#printing them on the image
height, width, _ = image.shape
for facial_landmarks in result.multi_face_landmarks:
    for i in landmarks_bottom:
        pt1 = facial_landmarks.landmark[i]
        x = int(pt1.x * width)
        y = int(pt1.y * height)
        cv2.circle(image, (x, y), 2, (255,255,255), -1)

cv2.imwrite("landmarks_reduced_bottom.jpg", image)