import setuptools

setuptools.setup(
    name="sltranslator",
    version="0.0.1",
    author="Seyed Ahmad Hosseini",
    author_email="seyed-ahmad.hosseini@inria.fr",
    description="3D Sign Language Translation",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
    install_requires=["numpy"]
)
