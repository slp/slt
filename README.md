# SLT

Sign Language Translation

Step1:

* [Sign Language Recognition, Generation, and Translation: An Interdisciplinary Perspective](https://www.researchgate.net/profile/Hernisa-Kacorri/publication/336793034_Sign_Language_Recognition_Generation_and_Translation_An_Interdisciplinary_Perspective/links/5dd3efe9299bf11ec85ffb59/Sign-Language-Recognition-Generation-and-Translation-An-Interdisciplinary-Perspective.pdf?origin=publication_detail)

* [Evaluating the Immediate Applicability of Pose Estimation for Sign Language Recognition](https://openaccess.thecvf.com/content/CVPR2021W/ChaLearn/papers/Moryossef_Evaluating_the_Immediate_Applicability_of_Pose_Estimation_for_Sign_Language_CVPRW_2021_paper.pdf)

* [RWTH-PHOENIX-Weather: A Large Vocabulary Sign Language Recognition and Translation Corpus](https://www.researchgate.net/profile/Oscar-Koller/publication/255389908_RWTH-PHOENIX-Weather_A_Large_Vocabulary_Sign_Language_Recognition_and_Translation_Corpus/links/57038e5d08aeade57a258f4d/RWTH-PHOENIX-Weather-A-Large-Vocabulary-Sign-Language-Recognition-and-Translation-Corpus.pdf?origin=publication_detail)

* [Content4All Open Research Sign Language Translation Datasets](https://arxiv.org/pdf/2105.02351.pdf)

* [Neural Sign Language Synthesis: Words Are Our Glosses](https://openaccess.thecvf.com/content_WACV_2020/papers/Zelinka_Neural_Sign_Language_Synthesis_Words_Are_Our_Glosses_WACV_2020_paper.pdf)

Step2:

* [Neural Sign Language Translation](https://openaccess.thecvf.com/content_cvpr_2018/papers/Camgoz_Neural_Sign_Language_CVPR_2018_paper.pdf)

* [Sign Language Transformers: Joint End-to-end Sign Language Recognition and Translation](https://openaccess.thecvf.com/content_CVPR_2020/papers/Camgoz_Sign_Language_Transformers_Joint_End-to-End_Sign_Language_Recognition_and_Translation_CVPR_2020_paper.pdf)

* [Neural Sign Language Synthesis: Words Are Our Glosses](https://openaccess.thecvf.com/content_WACV_2020/papers/Zelinka_Neural_Sign_Language_Synthesis_Words_Are_Our_Glosses_WACV_2020_paper.pdf)


Step3:

* [Progressive transformers for end-to-end sign language production](https://arxiv.org/pdf/2004.14874.pdf)

* [Looking for the Signs: Identifying Isolated Sign Instances in Continuous Video Footage](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9667037)

* [Continuous 3d multi-channel sign language production via progressive transformers and mixture density networks](https://link.springer.com/content/pdf/10.1007/s11263-021-01457-9.pdf)

* [A Simple Multi-Modality Transfer Learning Baseline for Sign Language Translation](https://arxiv.org/pdf/2203.04287.pdf)

* [Privacy-Preserving British Sign Language Recognition Using Deep Learning](https://scholar.google.co.uk/scholar_url?url=https://www.techrxiv.org/articles/preprint/Privacy-Preserving_British_Sign_Language_Recognition_Using_Deep_Learning/19170257/1/files/34062986.pdf&hl=en&sa=X&d=15128268005393623734&ei=zzYwYpuED42Sy9YPvbOEwAo&scisig=AAGBfm1_eVP-2XIhsGfTUHnWfjbhwrAiUQ&oi=scholaralrt&html=&pos=0&folt=rel)

* [Keypoint based Sign Language Translation without Glosses](https://arxiv.org/pdf/2204.10511.pdf)
