import json
import os
import matplotlib.pyplot as plt

two_d = False

with open('/home/ari/git/phd/global_gaps_3.json', 'r') as f:
    gaps = json.load(f)

with open('/home/ari/git/phd/global_error_rate_o.json', 'r') as f:
    data = json.load(f)

with open('/home/ari/git/slt/data/jsons/starts2.json', 'r') as f:
    wstart = json.load(f)

colors = {"A":'orange', "B":'pink', "C":'blue', "D":'brown', "E":'red', "F":'grey', "G":'yellow', "H":'green'}
path = "/media/ari/phd_drive/phd/signers"

plot_groups = {}
categ = {}

if not two_d:
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

if two_d:
    for c in colors.keys():
        plot_groups[c] = [[] for _ in range(6)]
else:
    for c in colors.keys():
        plot_groups[c] = [[] for _ in range(5)]

w_colors = {}
for cat in os.listdir(path):
    abs_path = os.path.join(path, cat)
    if not cat in categ:
        categ[cat] = []

    for sub_file in os.listdir(abs_path):
        abs_file = os.path.join(abs_path, sub_file)
        sub_link = sub_file.replace("_cropped.jpg", "")
        if sub_link in gaps:
            left_hand = gaps[sub_link]["left"]
            right_hand = gaps[sub_link]["right"]
            if sub_link in data:
                if not sub_link in w_colors:
                    w_colors[sub_link] = colors[cat]

                if two_d:
                    for left_gap_size in left_hand:
                        print (20*">",left_gap_size)
                        #categ[cat][sub_link] = data[sub_link]
                        plot_groups[cat][0].append(int(gaps[sub_link]["left"][left_gap_size]["range"]))#-data[sub_link]['left_hand_wirst'])
                        plot_groups[cat][1].append(int(left_gap_size))#-data[sub_link]['right_hand_wirst'])
                        plot_groups[cat][4].append(colors[cat])

                    for right_gap_size in right_hand:
                        print (20*">",right_gap_size)
                        plot_groups[cat][2].append(gaps[sub_link]["right"][right_gap_size]["range"])#-data[sub_link]['left_hand_wirst'])
                        plot_groups[cat][3].append(left_gap_size)#-data[sub_link]['right_hand_wirst'])
                        plot_groups[cat][5].append(colors[cat])
                else:
                    for left_gap_size in left_hand:
                        #categ[cat][sub_link] = data[sub_link]
                        #if gaps[sub_link]["left"][left_gap_size]["frames"][0][0] >= 25000:
                            #print (sub_link, cat, gaps[sub_link]["left"][left_gap_size]["frames"])
                        if int(left_gap_size)>25:
                            print (sub_link, cat, left_gap_size)
                            categ[cat].append(left_gap_size)
                            for f in gaps[sub_link]["left"][left_gap_size]["frames"]:
                                #print (f[0])#, int(left_gap_size), int(gaps[sub_link]["left"][left_gap_size]["range"]))
                                plot_groups[cat][0].append(int(gaps[sub_link]["left"][left_gap_size]["range"]))
                                plot_groups[cat][1].append(int(left_gap_size))
                                plot_groups[cat][2].append(int(f[0]))
                                plot_groups[cat][3].append(colors[cat])

for k in categ:
    print (len(categ[k]), k)

if two_d:
    for cat in plot_groups:
        plt.scatter(plot_groups[cat][0], plot_groups[cat][1], c=plot_groups[cat][4], marker="+", label="Signers: {}".format(cat))


    plt.title("Left Hand global gap")
    plt.xlabel('Gap Occurrence', fontsize=18)
    plt.ylabel('Gap Size', fontsize=16)
    plt.legend()
    #plt.savefig("/media/ari/phd_drive/phd/signers_plot/" + '{}_H_dec.png'.format(cat), dpi = 300)
    plt.show()
else:
    #print (len(plot_groups["A"][0]), len(plot_groups["A"][1]), len(plot_groups["A"][2]), len(plot_groups["A"][3]))
    #print (len(plot_groups["A"][2]), len(plot_groups["A"][3]), len(plot_groups["A"][5]))
    for cat in plot_groups:
        ax.scatter(plot_groups[cat][0],
                    plot_groups[cat][2],
                    plot_groups[cat][1],
                    marker="+",
                    c=plot_groups[cat][3],
                    label="Signers: {}".format(cat))

    for sample_id in wstart:
        #print (sample_id)
        if sample_id["name"] in w_colors.keys():
            if "TV-20190810-2034-0601" != sample_id['name']:
                #print (20*">>", int(sample_id["frame"]), sample_id)
                if int(sample_id["frame"])<15000:
                    print (int(sample_id["frame"]), sample_id)
                ankerpoint = [0, int(sample_id["frame"]), 600]
                an_x = [1, 1]
                an_y = [int(sample_id["frame"]),ankerpoint[1]]
                an_z = [200, 250]
                ax.plot(an_x,an_y,an_z, color = "b")

    ax.set_xlabel('Gap Occurrence')
    ax.set_ylabel('Gap Frame Ranges')
    ax.set_zlabel('Gap Size')
    plt.legend()
    plt.show()
