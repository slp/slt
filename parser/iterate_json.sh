echo "NAMES"
declare -i y=0
declare -i m=3
arr=()
jq -r '.[] | to_entries[] | (.value)' ../data/starts2.json | while read i; do
    # do stuff with $i
    arr+=($i)
    y=$y+1;
    if [ $y -eq $m ]
    then
        target_input=/media/ari/phd_drive/phd/data_collection/${arr[0]}/full.mp4
        if [ -f "$target_input" ]; then
          target_output=/home/ari/git/slt/data/weather/${arr[0]}.jpg
          echo $target_input
          echo $target_output
          echo time: ${arr[2]}
          ffmpeg -i $target_input -nostdin -ss ${arr[2]} -q:v 2 -vframes 1 $target_output || break
          echo --------------------------------------------------;
        fi
        declare -i y=0
        arr=()
    fi

done
