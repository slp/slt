import json
import os
import numpy as np
sentences_path = ""
pose_path = ""
sample_id = ""
a = "/home/ari/git/slt/data/train.sr"
b = "/home/ari/git/slt/data/train.ta"
c = "/home/ari/git/slt/data/signer_b.txt"
src  = open(a, "w")
tar  = open(b, "w")
merge  = open(c, "w")

with open('/home/ari/git/slt/data/jsons/total_sentences_incl_ends_3.json', 'r') as f:
  samples = json.load(f)
tar_sign = "/media/ari/phd_drive/phd/signers/B"
target_signers = os.listdir(tar_sign)
target_signers = [x.replace("_cropped.jpg", "") for x in target_signers]
match, mis_match = 0, 0
for sample_id in samples.keys():
    if sample_id in target_signers:
        load_pos = "/media/ari/phd_drive/phd/data_collection/{}/pose.txt".format(sample_id)
        with open(load_pos) as f:
            lines = f.readlines()
        for sen in samples[sample_id]:
            sen, start, end = "".join(i for i in sen[0]), sen[1][0], sen[1][1]
            print (sen, start, end)
            con = lines[int(start):int(end)]
            if len(con)==int(end-start):
                #src.write(sen.lower() + '\n')
                q = "Q: "+sen.lower()+'\n'
                con = [o.replace(" \n", "") for o in con]
                merge.write(q)
                #con_can = "".join(o for o in con)
                #print (con_can)
                #tar.write(con_can+ '\n')
                con_can = ""
                for xx in con:
                    #print (xx, type(xx))
                    p_arr = np.array([float(_val) for _val in xx.split(" ")])
                    p_x = p_arr[0::3]
                    p_y = p_arr[1::3]
                    p_z = p_arr[2::3]
                    con_can += str(p_x[15])+" "+str(p_y[15])+" "+str(p_z[15])+" "+str(p_x[16])+" "+str(p_y[16])+" "+str(p_z[16])
                q_a = "A: "+con_can+"\n"

                print (q_a, int(end-start)*6)
                merge.write(q_a)
                merge.write("===\n")
                match += 1
            else:
                mis_match += 1
print(match, mis_match)
